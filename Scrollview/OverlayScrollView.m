//
//  OverlayScrollView.m
//  Scrollview
//
//  Created by Benoit on 24/09/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "OverlayScrollView.h"

@implementation OverlayScrollView

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *hitview = [super hitTest:point withEvent:event];
    if (hitview == self) {
        return nil;
    }
    
    return hitview;
}

@end
