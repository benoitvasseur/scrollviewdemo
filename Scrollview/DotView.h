//
//  DotView.h
//  Scrollview
//
//  Created by Benoit on 24/09/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DotView : UIView

+ (DotView *)randomDotView;
+ (void)arrangeDotsRandomlyInView:(UIView *)view;
+ (void)arrangeDotsNeatlyInView:(UIView *)view withAnimation:(BOOL)withAnimation;

@end
