//
//  TouchDelayGestureRecognizer.m
//  Scrollview
//
//  Created by Benoit on 24/09/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "TouchDelayGestureRecognizer.h"
#import <UIkit/UIGestureRecognizerSubclass.h>

@implementation TouchDelayGestureRecognizer {
    NSTimer *_timer;
}

- (instancetype)initWithTarget:(id)target action:(SEL)action {
    self = [super initWithTarget:target action:action];
    if (self) {
        self.delaysTouchesBegan = YES;
    }
    
    return self;
}

- (void)fail {
    self.state = UIGestureRecognizerStateFailed;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [self fail];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self fail];
}

- (void)reset {
    [_timer invalidate];
    _timer = nil;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.15 target:self selector:@selector(fail) userInfo:nil repeats:NO];
}

@end
